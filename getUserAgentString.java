public synchronized String getUserAgentString() {
    if (DESKTOP_USERAGENT.equals(mUserAgent) ||
            IPHONE_USERAGENT.equals(mUserAgent) ||
            !mUseDefaultUserAgent) {
        return mUserAgent;
    }

    boolean doPostSync = false;
    synchronized(sLockForLocaleSettings) {
       Locale currentLocale = Locale.getDefault();
       if (!sLocale.equals(currentLocale)) {
           sLocale = currentLocale;
           mUserAgent = getCurrentUserAgent();
           mAcceptLanguage = getCurrentAcceptLanguage();
           doPostSync = true;
       }
   }
   if (doPostSync) {
       postSync();
   }
   return mUserAgent;
}

private synchronized String getCurrentUserAgent() {
    Locale locale;
    synchronized(sLockForLocaleSettings) {
        locale = sLocale;
    }
    StringBuffer buffer = new StringBuffer();
    // Add version
    final String version = Build.VERSION.RELEASE;
    if (version.length() > 0) {
        buffer.append(version);
    } else {
        // default to "1.0"
        buffer.append("1.0");
    }  
    buffer.append("; ");
    final String language = locale.getLanguage();
    if (language != null) {
        buffer.append(language.toLowerCase());
        final String country = locale.getCountry();
        if (country != null) {
            buffer.append("-");
            buffer.append(country.toLowerCase());
        }
    } else {
        // default to "en"
        buffer.append("en");
    }
    
    final String model = Build.MODEL;
    if (model.length() > 0) {
        buffer.append("; ");
        buffer.append(model);
    }
    final String id = Build.ID;
    if (id.length() > 0) {
        buffer.append(" Build/");
        buffer.append(id);
    }
    final String base = mContext.getResources().getText(
            com.android.internal.R.string.web_user_agent).toString();
    return String.format(base, buffer);
}
